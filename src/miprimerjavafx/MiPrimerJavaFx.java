package miprimerjavafx;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author diurno2020
 */
public class MiPrimerJavaFx extends Application{
    
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("VentanaFXML.fxml"));
        stage.setTitle("Hola Mundi");
        stage.setScene(new Scene(root));
        stage.show();
    }
}